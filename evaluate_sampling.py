#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 09:19:08 2022

@author: bschmitz
"""

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

#%%
data_resampled = pd.read_csv('./neutronyield-data-resampled.csv')
data = pd.read_csv('./neutronyield-data.csv')
#%% Import sample data for energy binning
test = pd.read_csv('./ExampleData.csv')
test['e-mean'] = (test['e-lower'] + test['e-upper'])/2

#%% defined indexi

ind = 3000

x0 = test['e-mean']
y0 = data.iloc[ind,7:]

xr = np.linspace(0,1,100)
yr = data_resampled.iloc[ind,7:107]

if False:
     # Identify the last index where still data is nonzero:
    inds = test[test['neutron'] != 0 ]
    # if inds.shape[0] > 4:
    #     # Cut parts where the relative error is 100%, larger error is not possible by design
    #     # only if enough data exists.
    #     inds = inds[inds['r.err'] != 1]
    # calculate mean value for the bins.
    inds['e-mean'] = (inds['e-lower'] + inds['e-upper'])/2
    
    # Redo binning to deal with zero values:
    # maximum bin energy
    e_max = np.max(inds['e-upper'])
    # normalze to max energy.
    x = inds['e-mean']/e_max
    y = inds['neutron']
    
    # add Emax value
    #Params1['Emax'] = e_max
    # Params2['Emax'] = e_max
    
    # new sampling points
    x_new = np.linspace(np.min(x),np.max(x),100)
    
    # Idea: interpolate the spectrum:
    if x.shape[0] >= 4:
        new_y = interp1d(x, y, kind='cubic')
        y_new = new_y(x_new)
    elif x.shape[0] <= 4:
        y_new = np.zeros(x_new.shape)

#%% Plot stuff
plt.plot(x0,y0)
plt.plot(xr*data_resampled['Emax'].iloc[ind],yr)
plt.yscale('log')
plt.xscale('log')