#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 29 09:42:56 2022

@author: bschmitz
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#%% import the simulation data:
data = pd.read_csv('./neutronyield-data.csv')
emean = pd.read_csv('./emean.csv')
# create energy mesh
datasetx = emean['e-mean']

#%%
Energies = [7,45,85] # MeV
Angles = [2.5,90] # degree
length = 82.2

exportdata = {}

for proj in ['deuter','proton']:
    deuterdata = data[data['Projectile']==proj]
    for targ in ['Li','Be']:
        setd=deuterdata[deuterdata['Element']==targ]
        for Ei in Energies :
            for theta in Angles:
                tempdata = setd[setd['Energy']==Ei]
                tempdata = tempdata[tempdata['Angle']==theta]
                length = max(set(tempdata['Length']))
                tempdata = tempdata[tempdata['Length']==length]
                    
                key= proj+'_'+targ+'_'+str(Ei)+'_'+str(theta)+'_'+str(length)

                exportdata[key] = [np.array(datasetx),np.array(tempdata.iloc[:,7:])[0]]
                
len(exportdata)

#%% save to file 
# load pickle module
import pickle

# create a binary pickle file 
f = open("validcurves.pkl","wb")

# write the python object (dict) to pickle file
pickle.dump(exportdata,f)

# close file
f.close()