# Neutron-Data

[![License: Apache 2.0](https://img.shields.io/badge/License-cc-Attribution4.svg)](https://git.rwth-aachen.de/surrogat-models/neutron-data/-/blob/main/LICENSE)
<!-- [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6383868.svg)](https://doi.org/10.5281/zenodo.6383868) -->



## Table of contents
1. [Introduction](#introduction)
2. [Strucuture of Raw Data Files](#rawstructure)
3. [Strucuture of Folder Tree](#folderstructure)
4. [Known Problems](#bugs)
5. [Publications & Links](#publications)
6. [Contributers](#contributors)
7. [Funding](#funding)

## 1. Introduction <a name="introduction"></a>

This repository contains data for double differential neutron yield simulations.
It consists of the simulations scripts, the individual raw data, the postprocessing script and the joined data files.
More details are given in the structure chapter.

The data was calculated using the PHITS Monte Carlo code [1] on a 130 node Windows cluster at TEMF, TU Darmstadt. 
<!-- This data is the basis for the liquid jet modeling paper [3].-->
This dataset was created as part of the LOEWE NP research cluster at TU Darmstadt [2] and the work was done at the Fachgebiet Beschleunigerphysik at TU Darmstadt [3].


## 2. Structure of raw data files <a name="rawstructure"></a>

The individual simulations are saved in tally output files (.out). 
They are labeled by the detector position according to their position in the simulation.
Its output format is fully described in the PHITS manual [1] and implicitely given in the postprocessing scripts.
Each set of detectors is also accompanied by a `batch.out` which lists some basic numerical quantities of each simulation run.

The raw data files list the minimum and maximum bin energy, the count rate of neutrons for the energy and its correpsonding statistical uncertainty.

## 3. Structure of Folder Tree <a name="folderstructure"></a>

The data is ordered in different folders, which all belond to the simulations ecosystem. 
* `./PHITS-Data/X/Y/ZZZ.ZMeV/l.ll/*.out`: The PHITS-Data tree is separated into:
    * X: Element of the converter.
    * Y: Element of the projectile.
    * Z: Energy of the projectile in ZZZ.Z MeV
    * l: Converter length in ll.l mm.
* `./PHITS-Input/OS/`: Contains the full simulation script adjusted for two different operating systems. Due to the structure of the cluster used, some hacks to ease parallelization of all simulations had to be made. The Windows subdirectory is therefore the better maintained part. Due to the internal structure of PHITS and its subsequent parallelization, several subfolders have to be created and used. 
    * `energy/`: The kinetic energy of the projectile.
    * `particle/`: The type of the projectile. 
    * `geometry/`: The element of the geometry.
    * `converter/`: The length of a cylindrical neutron converter.
* `./Prepare PHITS Calc/`: Contains a python script to calculate all needed auxiliary files, mirroring the structure from above.
* `./bootstrap/`: Contains fully resampled spectra with a variation of the data following the statistical uncertainty of the data. These are needed to evaluate the resulting uncertainties on the calculated models. The files given here are following a dedicated scheme:
    * `*-sigma-Z`: If the name has a sigma inside its data variation is the same for each data point, its value has been modified by Z*standard deviation. The sign is relevant, the spectra then resemble lower and upper bounds of the uncertainty around the raw data.
    * `*-randomized-N`: If the file is indicated as randomized, then N indicates a simple counting value. The data is varied following a resampling of the data with a gaussian distribution. The gaussian's mean is the raw data value and the sigma is the standard eviation of the raw data. Resampling randomized from this function generates the randomized data.
* `./`: In the root directory the resulting data files, combining all datasets and scripts for the evaluation of the data are included.
    * `ImportPHITS.py`: The main postprocessing script. It can read the PHITS files in question and reads in everyting, sorts it into combined dataframes and exports to the corresponding csv files. It was created using spyder and has several cells, which calculate different parts of the needed data. The goal of each cell is listed in the corresponding comment.
    * `evaluate_sampling.py`: Script that allows for the comparison of the raw data spectra with resampled spectra. 
    * `extractVerificationData.py`: Script that writes out the raw data spectra data points of given data sets into a `validcurves.pkl`.
    * `success_data.txt`: A file that lists all parameter combinations for which calculation was started and successfully done. 
    * `missing_data.txt`:A  file that lists all parameter combinations for which calculation was started but not successfully done. Problems arose in the Monte Carlo code and some could be retraced to missing cross section in the used data library.
    * Some csv data files: 
        * `ExampleData.csv`: Extracted sample data for testing.
        * `emean.csv`: Mean value of the raw data's energy binning. Saved to reduce time during postprocessing.
        * `neutronyield-data.csv`: Combined results for the spectra data of all raw data files in a table structure as defined in the table below.
        * `neutronyield-sigma.csv`: Combined results for the uncertainties of all raw data files in a table structure as defined in the table below.
        * `neutronyield-data-resampled.csv`: Combined results for the resampled spectra data. Resampled using cubic spline interpolation and linear sampling applied. This was created to mitigate problems from the large sampling space in the high energy part of the spectra. The raw data was sampled logarithmically by the Monte Carlo code. 

| Parameters | Data Spectrum |
| ---         | ---           |
| The meta data and parameters of the simulations | Spectral data extracted from the simulations |
| 7 Parameters: Index, Element, Projectile, Energy, Length, Angle, AngleError | 400 datapoints for `neutronyield-data.csv` and `neutronyield-sigma.csv`, 300 datapoints for `neutronyield-data-resampled.csv`| 

## 4. Known Problems <a name="bugs"></a>
The PHITS code does run into infinity loops for several parameter combinations. The exact reason is not known. 

The simulation data assumes cylindrical converter targets, which have a constant radius of 2.5 cm. It can therefore not reconstruct the variation in the spectrum due to radius variation. 
Major problems might arise through the nuclear cross section database used (FENDL), since all data bases have some inherent bias. 
As soon as this database is not valid, then the use case should be recalculated and merged with the current data set.

## 5. Publications & Links <a name="publications"></a>

[1] [PHITS Code](https://phits.jaea.go.jp/)

<!-- [3] [Modeling of a Liquid Leaf Target TNSA Experiment Using Particle-In-Cell Simulations and Deep Learning](https://doi.org/10.1155/2023/2868112)-->

[2] [LOEWE center for Nuclear Photonics](https://www.ikp.tu-darmstadt.de/nuclearphotonics/nuclear_photonics/index.en.jsp) 

[3] [Accelerator Pyhsics at TEMF, TU Darmstadt](https://www.bp.tu-darmstadt.de/fachgebiet_beschleunigerphysik/index.en.jsp)

## 6. Contributors <a name="contributors"></a>
Contributors include (alphabetically): 
*   B. Schmitz

## 7. Funding <a name="funding"></a>
* This work was funded by HMWK through the LOEWE center “Nuclear Photonics”.
