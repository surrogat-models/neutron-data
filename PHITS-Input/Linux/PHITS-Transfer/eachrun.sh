#!/bin/bash

# Usage: Usage: eachrun.bat %1 %2 %3 %4
# %1: Particle file name
# %2: Energy file name
# %3: Converter Material and Density
# export OMP_NUM_THREADS=0

PHITSEXE="/home/bschmitz/phits/phits/bin/phits.sh"
OUTDIR="output/$3/$1/$2/$4"

# create output directory if it does not exist yet
if [ ! -d "$OUTDIR" ]
then
  mkdir -p output/$3
  mkdir -p output/$3/$1
  mkdir -p output/$3/$1/$2
  mkdir -p output/$3/$1/$2/$4
fi

echo Calculating $1 $2 $3 $4 ...

cp particle/$1.inp particle.tmp
cp energy/$2.inp energy.tmp
cp converter/$3.inp converter.tmp
cp geometry/$4.inp geometry.tmp

$PHITSEXE testing_yield.inp

mv *.out $OUTDIR/
mv det* $OUTDIR/

rm particle.tmp
rm energy.tmp
rm converter.tmp
rm geometry.tmp
