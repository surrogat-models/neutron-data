REM: Usage: eachrun.bat %1 %2 %3 %4
REM: %1: Particle file name
REM: %2: Energy file name
REM: %3: Converter Material and Density
REM: %4: Converter Thickness

echo Calculating %1 %2 %3 %4 ...

@echo off

SET PHITSBAT="C:\Users\schmitz\phits\bin\phits.bat"
md "\\temfcl2000\E\User\BS\PHITS-Output\output\%3\%1\%2\%4\"

copy C:\Users\schmitz\PHITS-Transfer\particle\%1.inp C:\Users\schmitz\PHITS-Transfer\particle.tmp
copy C:\Users\schmitz\PHITS-Transfer\energy\%2.inp C:\Users\schmitz\PHITS-Transfer\energy.tmp
copy C:\Users\schmitz\PHITS-Transfer\converter\%3.inp C:\Users\schmitz\PHITS-Transfer\converter.tmp
copy C:\Users\schmitz\PHITS-Transfer\geometry\%4.inp C:\Users\schmitz\PHITS-Transfer\geometry.tmp

call %PHITSBAT% testing_yield.inp

move C:\Users\schmitz\PHITS-Transfer\*.out \\temfcl2000\E\User\BS\PHITS-Output\output\%3\%1\%2\%4\
move C:\Users\schmitz\PHITS-Transfer\det* \\temfcl2000\E\User\BS\PHITS-Output\output\%3\%1\%2\%4\

del particle.tmp
del energy.tmp
del converter.tmp
del geometry.tmp
del param.tmp
