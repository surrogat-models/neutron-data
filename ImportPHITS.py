#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 14:24:11 2019
This file should allow to parse arbitrary PHITS data for displaying and analyzing them.

@author: benedikt
"""
# %% Import of neded modules and variables
## Modules
import re
import numpy as np
#import math
import array
#import matplotlib.pyplot as plt
import os
import csv
import pandas as pd
import tqdm
from scipy.interpolate import interp1d

## Variables
path = './'
RawData = path
# %% Definition of functions needed
def getInd(liste, exp):
    """
    This is a function for finding the index of an expression in a list using regular expressions
    """
    return [ i for i, word in enumerate(liste) if re.search(exp,word) ];

def import_PHITS(filename):
    with open(filename,'r') as f:
        Tempdata = f.readlines()
        return Tempdata;   

def create_mesh(block_00):
    """
    Function that reads out the mesh cell boundaries and 
    the corresponding meta data out of the first part of a PHITS Outputfile.
    Currently only on r-z cylindrical meshes.

    Parameters
    ----------
    block_00 : STR 
        Zero-th part of the PHITS input file to recontrsuct mesh.

    Returns
    -------
    meta_mesh : Dict
        Dictionary containing all meta information about the mesh.
    mesh_object : Dict of np arrays
        Contains arrays of the meshes boundary.

    """

    # define metadata to be saved:
    meta_data_list = [['x0','y0','z0','unit']]
    # Save the mes meta data into a dictionary, fill dictionary during parsing
    meta_mesh = {}
    # save the metadata's field name in to a temporary variable
    for line in block_00:
        # take each line in the initial block and evaluate it
        temp_line = line.split()
        # fill meta_data_list to get full number of relevant parameters
        if temp_line[0] =='mesh':
            if temp_line[2] == 'r-z':
                meta_data_list.append(['r-type','rmin','rmax','rdel','nr','z-type','zmin','zmax','zdel','nz'])
                # not sure how to check for e-type a-priori, but this is known to exist for all!
                meta_mesh[temp_line[0]] = temp_line[2]
                
            elif temp_line[2] == 'reg':
                # not sure how to check for e-type a-priori, but this is known to exist for all!
                meta_mesh[temp_line[0]] = temp_line[2]
                meta_data_list.append(['reg'])
                
            meta_data_list.append(['e-type','emin','emax','edel','ne'])
                
            # other mesh geometries can be entered here as well with an elif statement
            # at the end of this the meta data fields are all defined, then the list of lists has to be flattened
            meta_data_list = sum(meta_data_list, [])
        # fill the metadata fields accordingly
        elif temp_line[0] in meta_data_list:
            # value 0 is descriptor, 1 the equality sign, 2 the value
            meta_mesh[temp_line[0]] = float(temp_line[2])
    # meta data dictionary created.
    # now calculate mesh and binning values can either be read or calculated.
    # calculation is the quickest way now
    mesh_object = {}
    if meta_mesh['mesh'] == 'r-z':
        #if cylindrical energy mesh, create a meshstructure accordingly
        for types in ['r-type','z-type','e-type']:    
            if meta_mesh[types] == 2:
                start       = meta_mesh[str(types[0])+'min']
                stop        = meta_mesh[str(types[0])+'max']
                step_number = int(meta_mesh['n'+str(types[0])])
                #calculate missing step_size and save to meta array
                step_size   = (stop-start)/step_number
                meta_mesh[str(types[0])+'del'] = step_size
            elif meta_mesh[types] == 4:
                start       = meta_mesh[str(types[0])+'min']
                stop        = meta_mesh[str(types[0])+'max']
                step_size = meta_mesh[str(types[0])+'del']
                #calculate missing step_size and save to meta array
                step_number   = int((stop-start)/step_size)
                meta_mesh['n'+str(types[0])] = step_size
                
            mesh_object[types] = np.linspace(meta_mesh[str(types[0])+'min'], meta_mesh[str(types[0])+'max'], num=step_number+1, endpoint=True)            
    # return the created values
    return meta_mesh, mesh_object

def create_data_array(block_ij, number_energy_mesh, geometry_mesh='reg'):
    """
    Function to read in blocks from PHITS file and return a pandas dataframe.

    Parameters
    ----------
    block_ij : list of Str 
        Extract of the PHITS file.
    number_energy_mesh : int
        Number of energy mesh cells.

    Returns
    -------
    export_data : pandas Dataframe
        The Dataframe from the passed PHITS data block.

    """
    
    # initialising the column names taken from the file, together with mesh indicators
     # offset at which position the data column's head is
    if geometry_mesh == 'reg':
        index_head = 8
        column_names = sum([block_ij[index_head].split()[1:]], [])
    elif geometry_mesh == 'r-z':
        index_head   = 10
        column_names = sum([['z_mesh','r_mesh'],block_ij[index_head].split()[1:]], [])
    
    # preallocate an array
    if geometry_mesh == 'r-z':
        temp_array = np.zeros((number_energy_mesh,len(column_names)),dtype=float)
        # Example line for mesh indication
        #   no. =  2   iz surf =  1   ir  =  2
        # taking the actual data:   
        mesh_cell = block_ij[2].replace('=','').split() 
        for j in range(number_energy_mesh):
            temp_array[j,0]   = mesh_cell[5]
            temp_array[j,1]   = mesh_cell[7]
            temp_array[j,2:6] = block_ij[index_head+1+j].split()
            
    elif geometry_mesh == 'reg':
        temp_array = np.zeros((number_energy_mesh,len(column_names)),dtype=float)
        for j in range(number_energy_mesh):
            temp_array[j,:] = block_ij[index_head+1+j].split()

    export_data = pd.DataFrame(data=temp_array, columns=column_names)        
    return export_data
        
def read_phits_file(file_path):
    """
    

    Parameters
    ----------
    file_path : TYPE
        DESCRIPTION.

    Returns
    -------
    meta_mesh : TYPE
        DESCRIPTION.
    mesh_object : TYPE
        DESCRIPTION.
    output_data : TYPE
        DESCRIPTION.

    """
    # STEP 1: read in of the given file / path
    file_content = import_PHITS(filename=file_path)
    
    # STEP 2: Separate file along the #------ lines
    exp1 = re.compile('#--------*') # Regular Expression for the field that separates the different calculation regions
    temp_bound = getInd(file_content,exp1) # Found the indices to split the lists into equal parts.
    # First of the blocks gives the meta mesh information
    block_00 = file_content[0:temp_bound[0]-1]
    
    # STEP 3: Extract / create the meshobject
    meta_mesh, mesh_object = create_mesh(block_00)
    
    # STEP 4: Create the data arrays
    temp_data = []
    for marker in tqdm.tqdm(temp_bound):
        data_block = file_content[marker:marker+15+int(meta_mesh['ne'])]
        temp_data.append(create_data_array(block_ij = data_block, number_energy_mesh = int(meta_mesh['ne']),geometry_mesh='r-z'))
    output_data = pd.concat(temp_data)
    return meta_mesh, mesh_object, output_data

def read_phits_file_ringdetector(file_path):
    """
    

    Parameters
    ----------
    file_path : TYPE
        DESCRIPTION.

    Returns
    -------
    output_data : TYPE
        DESCRIPTION.

    """
    # STEP 1: read in of the given file / path
    file_content = import_PHITS(filename=file_path)
    
    # STEP 2: Separate file along the #------ lines
    exp1 = re.compile('#--------') # Regular Expression for the field that separates the different calculation regions
    temp_bound = getInd(file_content,exp1) # Found the indices to split the lists into equal parts.
    
    # STEP 4: Create the data arrays
    temp_data = []
    for marker in temp_bound:
        data_block = file_content[marker:marker+9+400]
        temp_data.append(create_data_array(block_ij = data_block, number_energy_mesh=400,geometry_mesh='reg'))
    output_data = pd.concat(temp_data)
    return output_data
    
def look_up_angle(detectornumber):
    
    if detectornumber == 27:
        DetRange = [175, 180]
    elif detectornumber == 26:
        DetRange = [170, 175]
    elif detectornumber == 22:
        DetRange = [165, 170]
    elif detectornumber == 21:
        DetRange = [155, 165]
    elif detectornumber == 20:
        DetRange = [145, 155]
    elif detectornumber == 19:
        DetRange = [135, 145]
    elif detectornumber == 18:
        DetRange = [125, 135]
    elif detectornumber == 17:
        DetRange = [115, 125]
    elif detectornumber == 16:
        DetRange = [105, 115]
    elif detectornumber == 15:
        DetRange = [95, 105]
    elif detectornumber == 14:
        DetRange = [85, 95]
    elif detectornumber == 13:
        DetRange = [75, 85]
    elif detectornumber == 12:
        DetRange = [65, 75]
    elif detectornumber == 11:
        DetRange = [55, 65]
    elif detectornumber == 10:
        DetRange = [45, 55]
    elif detectornumber == 9:
        DetRange = [35, 45]
    elif detectornumber == 8:
        DetRange = [25, 35]
    elif detectornumber == 7:
        DetRange = [15, 25]
    elif detectornumber == 6:
        DetRange = [10, 15]
    elif detectornumber == 5:
        DetRange = [5, 10]
    elif detectornumber == 1:
        DetRange = [0, 5]
        
    MiddleValue = np.sum(DetRange)/2.
    ErrorValue  = MiddleValue-DetRange[0] # error is symmetric due to the construction of the middle value
    #SolidAngle  = 2*np.pi*(np.cos() - np.cos())
    return MiddleValue, ErrorValue

#%%
if __name__ == "__main__":
    resample_data = False
    bootsbool = True
    
    
    
    if resample_data == False:
        
        # Preallocate dataframe: 
        Column_Names = ['Element','Projectile','Energy','Length','Angle','AngleError']
        for i in range(400):
            Column_Names.append('bin_'+str(i))
        # 2 dataframes are needed
        ## dataframe with the data
        df_data  = pd.DataFrame(columns=Column_Names)
        ## dataframe with the corresponding measurement uncertainties
        df_sigma = pd.DataFrame(columns=Column_Names)
        
        # Some simulations gave error messages This list will contain the problematic parameter combinations
        ErrorList = []    
        # List of all successfull data
        SuccessList = []
        
        # Definition of the root directory for the rawdata tree
        os.chdir(path)
        rootdir = 'PHITS-Data'
           
        exp0 = re.compile('det*') #Filter to extract the detector data files. 
        # First level: Element of converter
        Elements = os.listdir(RawData+rootdir)
        for ele in Elements:
            # Second level: Projectile type
            Projectiles = os.listdir(RawData+rootdir+'/'+ele)
            for proj in Projectiles:
                # Third level: Projectile energy
                Energy = os.listdir(RawData+rootdir+'/'+ele+'/'+proj)
                for en in tqdm.tqdm(Energy):
                    ConvLength = os.listdir(RawData+rootdir+'/'+ele+'/'+proj+'/'+en)
                    for ll in ConvLength:
                        # Fourth level: Converter length
                        Detectors = os.listdir(RawData+rootdir+'/'+ele+'/'+proj+'/'+en+'/'+ll)
                        if len(Detectors) >= 20:
                            # Fifth level: Detector angle is identified over the detector's number
                            FileIndex = getInd(Detectors,exp0) # extracts all positions at which the 'det*' scheme fits
                            Detectors = [Detectors[i] for i in FileIndex]
                            for dec in Detectors:              
                                # extract the detector number
                                decno = int(dec.split('-')[1].split('.')[0])
                                AngleValue, AngleError = look_up_angle(detectornumber=int(decno))
                                # extract neutron yield data
                                Test = read_phits_file_ringdetector(RawData+rootdir+'/'+ele+'/'+proj+'/'+en+'/'+ll+'/'+dec)
                                Params1 = {'Element':ele, 'Projectile': proj, 'Energy':float(en[0:-3]), 'Length':ll, 'Angle':AngleValue, 'AngleError':AngleError}
                                Params2 = {'Element':ele, 'Projectile': proj, 'Energy':float(en[0:-3]), 'Length':ll, 'Angle':AngleValue, 'AngleError':AngleError}
                                for i in range(400):
                                    Params1['bin_'+str(i)] = Test['neutron'].values[i]
                                    Params2['bin_'+str(i)] = Test['r.err'].values[i]
                                # Save to dataframe
                                df_data.loc[len(df_data.index)] = Params1
                                df_sigma.loc[len(df_sigma.index)] = Params2
                                
                            # List of the data
                            SuccessList.append((ele, proj, en, ll))
                        else:
                            # If the corresponding directory does not contain the necessary number of files, then add to list
                            ErrorList.append((ele, proj, en, ll))
        #%% Save data to file
        df_data.to_csv('neutronyield-data.csv')
        df_sigma.to_csv('neutronyield-sigma.csv')
        # save list with successfull data points
        with open('success_data.txt', 'w') as f:
            for tuple in SuccessList:
                f.write('%s %s %s %s\n' % tuple)
        # save list with problematic data points
        with open('missing_data.txt', 'w') as f:
            for tuple in ErrorList:
                f.write('%s %s %s %s\n' % tuple)

#%% Resampling for bootstrap if the original is already done, this saves time!
# This methods creates the full sigma displacement of the curves.

if bootsbool == True:
    df_data = pd.read_csv('neutronyield-data.csv')
    df_sigma = pd.read_csv('neutronyield-sigma.csv')
    
    # import the energy bin values:
    emeanvalue = pd.read_csv('emean.csv')
    # number of samples for resampling:
    nsp = 100
    
    Column_Names = ['Element','Projectile','Energy','Length','Angle','AngleError']
    for i in range(nsp):
        Column_Names.append('bin_'+str(i))
    #Column_Names.append('Emax')
    
    # step 1: variaton to sigma boundaries:
    sigmas = [-5,-3,-1,1,3,5]
    for sig in tqdm.tqdm(sigmas):
        df_datai = df_data.copy()
        df_datai.iloc[:,7:] = df_data.iloc[:,7:]+sig*df_sigma.iloc[:,7:]*df_data.iloc[:,7:]
        # save files to disk
        df_datai.to_csv('bootstrap/neutronyield-data-sigma_'+str(sig)+'.csv')
        
        
        # resample the ith iteration !
        #Colu = Column_Names[Column_Names!='Emax']
        df_resamp = df_data[Column_Names]
        df_resamp['Emax'] = 0 # add Emax!
        
        for i in range(df_resamp.shape[0]):
            data = np.array(df_datai.iloc[i,7:])
            inds = data!=0
            # check whether data exists.
            if np.sum(inds) > 0:
                # maximum bin energy 
                e_max = np.max(emeanvalue[inds]).values[1]
                # normalze to max energy.
                x = (emeanvalue['e-mean'][inds]/e_max).values
                y = data[inds]
                                            
                # add Emax value
                df_resamp.loc[i,'Emax'] = e_max
                # Params2['Emax'] = e_max
                                                
                # Resampling!
                # new sampling points        
                x_new = np.linspace(np.min(x), np.max(x), nsp)
                                
                # Idea: interpolate the spectrum:
                if x.shape[0] > 4:
                    new_y = interp1d(x, y, kind='cubic')
                    y_new = new_y(x_new)
                elif x.shape[0] <= 4:
                    y_new = np.zeros(x_new.shape)
                for i in range(nsp):
                    df_resamp.loc[i,'bin_'+str(i)] = y_new[i]
            else: 
                df_resamp.iloc[i,7:] = 0
        
        df_resamp.to_csv('bootstrap/neutronyield-data-resampled-sigma_'+str(sig)+'.csv')
        
#%% Resampling for bootstrap if the original is already done, this saves time!
# This methods creates randomized curve generations.

if bootsbool == True:
    df_data = pd.read_csv('neutronyield-data.csv')
    df_sigma = pd.read_csv('neutronyield-sigma.csv')
    
    # import the energy bin values:
    emeanvalue = pd.read_csv('emean.csv')
    # number of samples for resampling:
    nsp = 100
    
    Column_Names = ['Element','Projectile','Energy','Length','Angle','AngleError']
    for i in range(nsp):
        Column_Names.append('bin_'+str(i))
    #Column_Names.append('Emax')
    
    # step 1: calc randomized scalings
    n=9
    out = np.random.normal(0,1,size=(54768,400,n))
    for ni in tqdm.tqdm(range(n)):
        df_datai = df_data.copy()
        df_datai.iloc[:,7:] = df_data.iloc[:,7:]+out[:,:,ni]*df_sigma.iloc[:,7:]*df_data.iloc[:,7:]
        # save files to disk
        df_datai.to_csv('bootstrap/neutronyield-data-randomises-'+str(ni)+'.csv')
        
        
        # resample the ith iteration !
        #Colu = Column_Names[Column_Names!='Emax']
        df_resamp = df_data[Column_Names]
        df_resamp['Emax'] = 0 # add Emax!
        
        for i in range(df_resamp.shape[0]):
            data = np.array(df_datai.iloc[i,7:])
            inds = data!=0
            # check whether data exists.
            if np.sum(inds) > 0:
                # maximum bin energy 
                e_max = np.max(emeanvalue[inds]).values[1]
                # normalze to max energy.
                x = (emeanvalue['e-mean'][inds]/e_max).values
                y = data[inds]
                                            
                # add Emax value
                df_resamp.loc[i,'Emax'] = e_max
                # Params2['Emax'] = e_max
                                                
                # Resampling!
                # new sampling points        
                x_new = np.linspace(np.min(x), np.max(x), nsp)
                                
                # Idea: interpolate the spectrum:
                if x.shape[0] > 4:
                    new_y = interp1d(x, y, kind='cubic')
                    y_new = new_y(x_new)
                elif x.shape[0] <= 4:
                    y_new = np.zeros(x_new.shape)
                for i in range(nsp):
                    df_resamp.loc[i,'bin_'+str(i)] = y_new[i]
            else: 
                df_resamp.iloc[i,7:] = 0
        
        df_resamp.to_csv('bootstrap/neutronyield-data-resampled-randomises-'+str(ni)+'.csv')
        
#%% calculate bin mean values 

    if resample_data == True:
        # number of new sampling points
        nsp = 100
    
        Column_Names = ['Element','Projectile','Energy','Length','Angle','AngleError']
        for i in range(nsp):
            Column_Names.append('bin_'+str(i))
        Column_Names.append('Emax')
        # 2 dataframes are needed
        ## dataframe with the data
        df_data  = pd.DataFrame(columns=Column_Names)
        ## dataframe with the corresponding measurement uncertainties
        df_sigma = pd.DataFrame(columns=Column_Names)
        
        # Some simulations gave error messages This list will contain the problematic parameter combinations
        ErrorList = []    
        # List of all successfull data
        SuccessList = []
        
        # Definition of the root directory for the rawdata tree
        os.chdir(path)
        rootdir = 'PHITS-Data'
        
        exp0 = re.compile('det*') #Filter to extract the detector data files. 
        # First level: Element of converter
        Elements = os.listdir(RawData+rootdir)
        for ele in Elements:
            # Second level: Projectile type
            Projectiles = os.listdir(RawData+rootdir+'/'+ele)
            for proj in Projectiles:
                # Third level: Projectile energy
                Energy = os.listdir(RawData+rootdir+'/'+ele+'/'+proj)
                for en in Energy:
                    ConvLength = os.listdir(RawData+rootdir+'/'+ele+'/'+proj+'/'+en)
                    for ll in ConvLength:
                        # Fourth level: Converter length
                        Detectors = os.listdir(RawData+rootdir+'/'+ele+'/'+proj+'/'+en+'/'+ll)
                        if len(Detectors) >= 20:
                            # Fifth level: Detector angle is identified over the detector's number
                            FileIndex = getInd(Detectors,exp0) # extracts all positions at which the 'det*' scheme fits
                            Detectors = [Detectors[i] for i in FileIndex]
                            for dec in Detectors:              
                                # extract the detector number
                                decno = int(dec.split('-')[1].split('.')[0])
                                AngleValue, AngleError = look_up_angle(detectornumber=int(decno))
                                # extract neutron yield data
                                Test = read_phits_file_ringdetector(RawData+rootdir+'/'+ele+'/'+proj+'/'+en+'/'+ll+'/'+dec)
                                Params1 = {'Element':ele, 'Projectile': proj, 'Energy':float(en[0:-3]), 'Length':ll, 'Angle':AngleValue, 'AngleError':AngleError}
                                # Params2 = {'Element':ele, 'Projectile': proj, 'Energy':float(en[0:-3]), 'Length':ll, 'Angle':AngleValue, 'AngleError':AngleError}
                                
                                # Identify the last index where still data is nonzero:
                                inds = Test[Test['neutron'] != 0 ]
                                # if inds.shape[0] > 4:
                                #     # Cut parts where the relative error is 100%, larger error is not possible by design
                                #     # only if enough data exists.
                                #     inds = inds[inds['r.err'] != 1]
                                # calculate mean value for the bins.
                                inds['e-mean'] = (inds['e-lower'] + inds['e-upper'])/2
                                
                                # Redo binning to deal with zero values:
                                # maximum bin energy 
                                e_max = np.max(inds['e-upper'])
                                # normalze to max energy.
                                x = inds['e-mean']/e_max
                                y = inds['neutron']
                                
                                # add Emax value
                                Params1['Emax'] = e_max
                                # Params2['Emax'] = e_max
                                
                                
                                # Resampling!
                                # new sampling points
                                x_new = np.linspace(np.min(x),np.max(x),nsp)
                                
                                # Idea: interpolate the spectrum:
                                if x.shape[0] >= 4:
                                    new_y = interp1d(x, y, kind='cubic')
                                    y_new = new_y(x_new)
                                elif x.shape[0] <= 4:
                                    y_new = np.zeros(x_new.shape)
                                
                                for i in range(nsp):
                                    Params1['bin_'+str(i)] = y_new[i]
                                    # Params2['bin_'+str(i)] = Test['r.err'].values[i]
                                # Save to dataframe
                                df_data.loc[len(df_data.index)] = Params1
                                # df_sigma.loc[len(df_sigma.index)] = Params2
                                
                            # List of the data
                            SuccessList.append((ele, proj, en, ll))
                        else:
                            # If the corresponding directory does not contain the necessary number of files, then add to list
                            ErrorList.append((ele, proj, en, ll))
        
        else:                     
            df_data.to_csv('neutronyield-data-resampled.csv')
            # df_sigma.to_csv('neutronyield-sigma.csv')
            # save list with successfull data points
            with open('success_data.txt', 'w') as f:
                for tuple in SuccessList:
                    f.write('%s %s %s %s\n' % tuple)
            # save list with problematic data points
            with open('missing_data.txt', 'w') as f:
                for tuple in ErrorList:
                    f.write('%s %s %s %s\n' % tuple)
    
    
        
    
        
        
