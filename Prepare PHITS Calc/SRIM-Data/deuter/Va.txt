 ==================================================================
              SRIM version ---> SRIM-2013.00
              Calc. date   ---> Februar 01, 2021 
 ==================================================================

 Disk File Name = SRIM Outputs\Deuteron in Vanadium.txt

 Ion = Hydrogen [1] , Mass = 2,016 amu

 Target Density =  5,9600E+00 g/cm3 = 7,0455E+22 atoms/cm3
 ======= Target  Composition ========
    Atom   Atom   Atomic    Mass     
    Name   Numb   Percent   Percent  
    ----   ----   -------   -------  
      V     23    100,00    100,00   
 ====================================
 Bragg Correction = 0,00%
 Stopping Units =  keV / micron   
 See bottom of Table for other Stopping units 

        Ion        dE/dx      dE/dx     Projected  Longitudinal   Lateral
       Energy      Elec.      Nuclear     Range     Straggling   Straggling
  --------------  ---------- ---------- ----------  ----------  ----------
999,999 eV    2,219E+01  5,974E+00     100 A       135 A       101 A   
   1,10 keV   2,327E+01  5,939E+00     109 A       144 A       107 A   
   1,20 keV   2,430E+01  5,899E+00     119 A       153 A       114 A   
   1,30 keV   2,530E+01  5,854E+00     128 A       161 A       121 A   
   1,40 keV   2,625E+01  5,807E+00     137 A       169 A       127 A   
   1,50 keV   2,717E+01  5,758E+00     146 A       177 A       134 A   
   1,60 keV   2,806E+01  5,708E+00     156 A       185 A       140 A   
   1,70 keV   2,893E+01  5,657E+00     165 A       193 A       146 A   
   1,80 keV   2,977E+01  5,606E+00     174 A       201 A       152 A   
   2,00 keV   3,138E+01  5,503E+00     193 A       215 A       164 A   
   2,25 keV   3,328E+01  5,376E+00     216 A       233 A       178 A   
   2,50 keV   3,508E+01  5,252E+00     240 A       249 A       192 A   
   2,75 keV   3,679E+01  5,133E+00     263 A       265 A       205 A   
   3,00 keV   3,843E+01  5,018E+00     287 A       281 A       218 A   
   3,25 keV   4,000E+01  4,908E+00     310 A       296 A       230 A   
   3,50 keV   4,151E+01  4,803E+00     333 A       310 A       242 A   
   3,75 keV   4,297E+01  4,702E+00     357 A       324 A       254 A   
   4,00 keV   4,437E+01  4,606E+00     380 A       338 A       266 A   
   4,50 keV   4,696E+01  4,427E+00     427 A       363 A       288 A   
   5,00 keV   4,939E+01  4,262E+00     474 A       387 A       309 A   
   5,50 keV   5,170E+01  4,111E+00     520 A       410 A       330 A   
   6,00 keV   5,390E+01  3,972E+00     567 A       432 A       350 A   
   6,50 keV   5,601E+01  3,844E+00     613 A       453 A       369 A   
   7,00 keV   5,804E+01  3,725E+00     659 A       473 A       387 A   
   8,00 keV   6,188E+01  3,511E+00     750 A       509 A       423 A   
   9,00 keV   6,548E+01  3,324E+00     840 A       543 A       456 A   
  10,00 keV   6,888E+01  3,159E+00     929 A       575 A       487 A   
  11,00 keV   7,210E+01  3,012E+00    1016 A       604 A       517 A   
  12,00 keV   7,516E+01  2,880E+00    1103 A       631 A       545 A   
  13,00 keV   7,809E+01  2,762E+00    1188 A       656 A       572 A   
  14,00 keV   8,091E+01  2,654E+00    1272 A       680 A       598 A   
  15,00 keV   8,361E+01  2,556E+00    1356 A       703 A       622 A   
  16,00 keV   8,622E+01  2,466E+00    1438 A       724 A       646 A   
  17,00 keV   8,874E+01  2,383E+00    1519 A       744 A       669 A   
  18,00 keV   9,118E+01  2,306E+00    1599 A       763 A       691 A   
  20,00 keV   9,583E+01  2,168E+00    1756 A       798 A       732 A   
  22,50 keV   1,013E+02  2,021E+00    1948 A       838 A       780 A   
  25,00 keV   1,064E+02  1,895E+00    2135 A       874 A       824 A   
  27,50 keV   1,112E+02  1,786E+00    2317 A       906 A       865 A   
  30,00 keV   1,157E+02  1,691E+00    2494 A       936 A       904 A   
  32,50 keV   1,200E+02  1,606E+00    2668 A       963 A       940 A   
  35,00 keV   1,241E+02  1,531E+00    2837 A       988 A       974 A   
  37,50 keV   1,280E+02  1,463E+00    3004 A      1011 A      1006 A   
  40,00 keV   1,318E+02  1,402E+00    3167 A      1033 A      1036 A   
  45,00 keV   1,387E+02  1,295E+00    3484 A      1072 A      1093 A   
  50,00 keV   1,451E+02  1,206E+00    3792 A      1107 A      1144 A   
  55,00 keV   1,510E+02  1,129E+00    4090 A      1138 A      1192 A   
  60,00 keV   1,565E+02  1,063E+00    4380 A      1166 A      1236 A   
  65,00 keV   1,616E+02  1,005E+00    4663 A      1191 A      1277 A   
  70,00 keV   1,663E+02  9,532E-01    4940 A      1215 A      1315 A   
  80,00 keV   1,746E+02  8,661E-01    5477 A      1257 A      1386 A   
  90,00 keV   1,819E+02  7,952E-01    5996 A      1295 A      1449 A   
 100,00 keV   1,881E+02  7,361E-01    6500 A      1328 A      1507 A   
 110,00 keV   1,935E+02  6,860E-01    6992 A      1357 A      1561 A   
 120,00 keV   1,980E+02  6,429E-01    7474 A      1384 A      1611 A   
 130,00 keV   2,019E+02  6,054E-01    7949 A      1409 A      1658 A   
 140,00 keV   2,051E+02  5,725E-01    8417 A      1432 A      1702 A   
 150,00 keV   2,078E+02  5,433E-01    8880 A      1454 A      1744 A   
 160,00 keV   2,099E+02  5,172E-01    9339 A      1474 A      1785 A   
 170,00 keV   2,116E+02  4,937E-01    9794 A      1494 A      1823 A   
 180,00 keV   2,129E+02  4,725E-01    1,02 um     1512 A      1861 A   
 200,00 keV   2,143E+02  4,355E-01    1,12 um     1549 A      1932 A   
 225,00 keV   2,145E+02  3,974E-01    1,23 um     1594 A      2017 A   
 250,00 keV   2,133E+02  3,659E-01    1,34 um     1636 A      2098 A   
 275,00 keV   2,110E+02  3,394E-01    1,46 um     1676 A      2176 A   
 300,00 keV   2,080E+02  3,168E-01    1,57 um     1714 A      2253 A   
 325,00 keV   2,044E+02  2,973E-01    1,69 um     1752 A      2328 A   
 350,00 keV   2,004E+02  2,802E-01    1,81 um     1790 A      2403 A   
 375,00 keV   1,963E+02  2,651E-01    1,94 um     1827 A      2478 A   
 400,00 keV   1,920E+02  2,517E-01    2,06 um     1864 A      2553 A   
 450,00 keV   1,833E+02  2,289E-01    2,33 um     1956 A      2705 A   
 500,00 keV   1,749E+02  2,101E-01    2,60 um     2050 A      2860 A   
 550,00 keV   1,669E+02  1,944E-01    2,89 um     2146 A      3020 A   
 600,00 keV   1,595E+02  1,810E-01    3,19 um     2246 A      3185 A   
 650,00 keV   1,527E+02  1,695E-01    3,51 um     2349 A      3356 A   
 700,00 keV   1,465E+02  1,595E-01    3,84 um     2455 A      3534 A   
 800,00 keV   1,356E+02  1,428E-01    4,54 um     2766 A      3909 A   
 900,00 keV   1,266E+02  1,295E-01    5,29 um     3087 A      4311 A   
   1,00 MeV   1,191E+02  1,186E-01    6,10 um     3417 A      4739 A   
   1,10 MeV   1,128E+02  1,095E-01    6,96 um     3754 A      5191 A   
   1,20 MeV   1,074E+02  1,018E-01    7,86 um     4099 A      5666 A   
   1,30 MeV   1,027E+02  9,519E-02    8,80 um     4450 A      6162 A   
   1,40 MeV   9,865E+01  8,943E-02    9,79 um     4807 A      6678 A   
   1,50 MeV   9,507E+01  8,436E-02   10,81 um     5168 A      7213 A   
   1,60 MeV   9,189E+01  7,988E-02   11,87 um     5533 A      7764 A   
   1,70 MeV   8,904E+01  7,588E-02   12,97 um     5901 A      8331 A   
   1,80 MeV   8,646E+01  7,228E-02   14,10 um     6273 A      8913 A   
   2,00 MeV   8,197E+01  6,608E-02   16,46 um     7405 A      1,01 um  
   2,25 MeV   7,670E+01  5,976E-02   19,59 um     8989 A      1,17 um  
   2,50 MeV   7,168E+01  5,460E-02   22,94 um     1,05 um     1,34 um  
   2,75 MeV   6,759E+01  5,032E-02   26,50 um     1,21 um     1,51 um  
   3,00 MeV   6,402E+01  4,669E-02   30,28 um     1,36 um     1,70 um  
   3,25 MeV   6,088E+01  4,358E-02   34,26 um     1,51 um     1,89 um  
   3,50 MeV   5,808E+01  4,088E-02   38,44 um     1,66 um     2,09 um  
   3,75 MeV   5,557E+01  3,851E-02   42,81 um     1,82 um     2,30 um  
   4,00 MeV   5,330E+01  3,642E-02   47,38 um     1,98 um     2,51 um  
   4,50 MeV   4,936E+01  3,288E-02   57,08 um     2,49 um     2,97 um  
   5,00 MeV   4,605E+01  3,000E-02   67,51 um     2,98 um     3,45 um  
   5,50 MeV   4,321E+01  2,761E-02   78,66 um     3,46 um     3,96 um  
   6,00 MeV   4,075E+01  2,559E-02   90,51 um     3,94 um     4,50 um  
   6,50 MeV   3,859E+01  2,386E-02  103,06 um     4,42 um     5,06 um  
   7,00 MeV   3,668E+01  2,236E-02  116,29 um     4,90 um     5,65 um  
   8,00 MeV   3,344E+01  1,989E-02  144,71 um     6,52 um     6,91 um  
   9,00 MeV   3,079E+01  1,794E-02  175,73 um     8,05 um     8,26 um  
  10,00 MeV   2,858E+01  1,635E-02  209,30 um     9,55 um     9,72 um  
  11,00 MeV   2,670E+01  1,503E-02  245,34 um    11,04 um    11,26 um  
  12,00 MeV   2,508E+01  1,392E-02  283,83 um    12,54 um    12,90 um  
  13,00 MeV   2,366E+01  1,297E-02  324,71 um    14,06 um    14,62 um  
  14,00 MeV   2,242E+01  1,214E-02  367,94 um    15,59 um    16,44 um  
  15,00 MeV   2,131E+01  1,142E-02  413,50 um    17,15 um    18,34 um  
  16,00 MeV   2,032E+01  1,079E-02  461,36 um    18,73 um    20,32 um  
  17,00 MeV   1,943E+01  1,022E-02  511,47 um    20,33 um    22,39 um  
  18,00 MeV   1,863E+01  9,715E-03  563,82 um    21,96 um    24,54 um  
  20,00 MeV   1,722E+01  8,845E-03  675,08 um    27,63 um    29,08 um  
  22,50 MeV   1,576E+01  7,964E-03  826,29 um    35,68 um    35,21 um  
  25,00 MeV   1,455E+01  7,249E-03  990,75 um    43,37 um    41,82 um  
  27,50 MeV   1,354E+01  6,657E-03    1,17 mm    50,94 um    48,90 um  
  30,00 MeV   1,267E+01  6,158E-03    1,36 mm    58,49 um    56,44 um  
  32,50 MeV   1,192E+01  5,732E-03    1,56 mm    66,09 um    64,43 um  
  35,00 MeV   1,126E+01  5,364E-03    1,78 mm    73,76 um    72,86 um  
  37,50 MeV   1,068E+01  5,042E-03    2,00 mm    81,54 um    81,72 um  
  40,00 MeV   1,016E+01  4,758E-03    2,24 mm    89,42 um    91,01 um  
  45,00 MeV   9,281E+00  4,280E-03    2,76 mm   117,28 um   110,84 um  
  50,00 MeV   8,556E+00  3,893E-03    3,31 mm   143,68 um   132,30 um  
  55,00 MeV   7,948E+00  3,572E-03    3,92 mm   169,55 um   155,33 um  
  60,00 MeV   7,431E+00  3,302E-03    4,57 mm   195,32 um   179,90 um  
  65,00 MeV   6,985E+00  3,072E-03    5,26 mm   221,18 um   205,96 um  
  70,00 MeV   6,596E+00  2,873E-03    5,99 mm   247,27 um   233,49 um  
  80,00 MeV   5,950E+00  2,546E-03    7,59 mm   339,64 um   292,81 um  
  90,00 MeV   5,435E+00  2,289E-03    9,34 mm   426,13 um   357,62 um  
 100,00 MeV   5,013E+00  2,080E-03   11,25 mm   510,61 um   427,70 um  
 110,00 MeV   4,662E+00  1,908E-03   13,31 mm   594,64 um   502,85 um  
 120,00 MeV   4,363E+00  1,763E-03   15,52 mm   678,97 um   582,90 um  
 130,00 MeV   4,107E+00  1,639E-03   17,88 mm   763,99 um   667,67 um  
 140,00 MeV   3,885E+00  1,532E-03   20,37 mm   849,91 um   757,02 um  
 150,00 MeV   3,689E+00  1,439E-03   23,01 mm   936,84 um   850,80 um  
 160,00 MeV   3,517E+00  1,357E-03   25,78 mm     1,02 mm   948,86 um  
 170,00 MeV   3,362E+00  1,284E-03   28,68 mm     1,11 mm     1,05 mm  
 180,00 MeV   3,224E+00  1,219E-03   31,71 mm     1,20 mm     1,16 mm  
 200,00 MeV   2,986E+00  1,107E-03   38,13 mm     1,53 mm     1,38 mm  
 225,00 MeV   2,745E+00  9,940E-04   46,85 mm     1,99 mm     1,68 mm  
 250,00 MeV   2,548E+00  9,027E-04   56,28 mm     2,42 mm     2,01 mm  
 275,00 MeV   2,385E+00  8,273E-04   66,39 mm     2,84 mm     2,35 mm  
 300,00 MeV   2,248E+00  7,640E-04   77,16 mm     3,25 mm     2,71 mm  
 325,00 MeV   2,131E+00  7,100E-04   88,56 mm     3,66 mm     3,09 mm  
 350,00 MeV   2,029E+00  6,633E-04  100,55 mm     4,06 mm     3,49 mm  
 375,00 MeV   1,941E+00  6,227E-04  113,12 mm     4,47 mm     3,90 mm  
 400,00 MeV   1,863E+00  5,868E-04  126,24 mm     4,87 mm     4,32 mm  
 450,00 MeV   1,732E+00  5,267E-04  154,02 mm     6,32 mm     5,22 mm  
 500,00 MeV   1,626E+00  4,781E-04  183,75 mm     7,66 mm     6,16 mm  
-----------------------------------------------------------
 Multiply Stopping by        for Stopping Units
 -------------------        ------------------
  1,0000E-01                 eV / Angstrom 
  1,0000E+00                keV / micron   
  1,0000E+00                MeV / mm       
  1,6779E-03                keV / (ug/cm2) 
  1,6779E-03                MeV / (mg/cm2) 
  1,6779E+00                keV / (mg/cm2) 
  1,4193E-01                 eV / (1E15 atoms/cm2)
  5,7753E-02                L.S.S. reduced units
 ==================================================================
 (C) 1984,1989,1992,1998,2008 by J.P. Biersack and J.F. Ziegler
