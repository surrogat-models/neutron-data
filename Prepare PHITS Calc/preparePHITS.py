#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 14:45:39 2022

@author: benedikt
"""

def srim_convert_energy(value, unit, energyprecision=(1.,"keV")):
    """
    Converts string prefixes into ints numerical equivalent in keV according to the precision tuple.
    Step size is given by the first element of the tuple, the unit by the second element.
    by Markus Hesse and Benedikt Schmitz

    Parameters
    ----------
    value : ?
        numerical value.
    unit : str
        prefactor of the numerical values.
    energyprecision : tuple (float, str)
	precision of the interpolation later, numerical divider and unit needed as tuple.

    Returns
    -------
    converted energy float
        The energy value converted to keV.

    """
    prefix = {"eV": 0.001, "keV": 1, "MeV": 1000}
    
    return float(value.replace(',','.'))*(prefix[unit.rstrip()]/prefix[energyprecision[1]])*energyprecision[0]

def srim_convert_distance(value, unit, precision = (.1,"um")):
    """
    Converts string prefixes into ints numerical equivalent according to the precision tuple.
    Step size is given by the first element of the tuple, the unit by the second element.
    by Markus Hesse and Benedikt Schmitz

    Parameters
    ----------
    value : ?
        numerical value.
    unit : str
        prefactor of the numerical values.
    precision : tuple (float, str)
	precision of the interpolation later, numerical divider and unit needed as tuple.

    Returns
    -------
    converted distance float
        The distance value converted to microns.

    """
    
    prefix = {"A": 0.0001, "um": 1, "mm": 1000, "m": 1000000}
    
    # First prefix factor gives the unit in microns
    # Second prefix factor converts to the needed precision 
    return float(value.replace(',','.'))*(prefix[unit.rstrip()]/prefix[precision[1]])#*precision[0]

def srim_readline(line,eprecision,xprecision):
    """
    Converts a line from an SRIM File to an array for usage.
    by Markus Hesse

    Parameters
    ----------
    line : char/str
        The line of the SRIM file.

    Returns
    -------
    line_array : float array
        Line formatted as array.

    """
    
    # Each line's format:
    # 0-7:   Ion Energy
    # 8-11:  Ion Energy Unit
    # 12-23: dE/dx Elec.
    # 24-34: dE/dx Nuclear
    # 35-41: Projected Range
    # 43-44: Projected Range Unit
    # 46-53: Long. Straggling
    # 55-56: Long. Straggling Unit
    # 58-65: Lat. Straggling
    # 67-68: Lat. Straggling Unit
    
    # We want to convert everything to keV or micron!
    # Create empty data array
    line_array = []
    
    # Convert energy to keV
    line_array.append(srim_convert_energy(line[0:7], line[8:12], energyprecision = eprecision))
    
    # Convert dE/dx 
    ## Given in keV/um
    Eprefix = {"eV": 0.001, "keV": 1, "MeV": 1000}
    Xprefix = {"A": 0.0001, "um": 1, "mm": 1000, "m": 1000000}

    correction = ((1./Eprefix[eprecision[1]])*eprecision[0])/((1./Xprefix[xprecision[1]])*xprecision[0])
    
    line_array.append(float(line[12:24].replace(',','.'))/ correction) 
    line_array.append(float(line[24:35].replace(',','.'))/ correction) 
    
    # Convert Ranges
    line_array.append(srim_convert_distance(line[35:42], line[43:45], precision = xprecision))
    line_array.append(srim_convert_distance(line[46:54], line[55:57], precision = xprecision))
    line_array.append(srim_convert_distance(line[58:66], line[67:69], precision = xprecision))
    
    # Return line in format:
    # Ion Energy, dE/dx Elec. dE/dx Nuclear, Proj. Range, Long. Straggling, Lat. Straggling
    return line_array

def srim_import(filename, eprecision, xprecision):
    """
    Function to import a given SRIM file and save it to a usable array.

    Parameters
    ----------
    filename : str
        Path to the corresponding SRIM file.

    Raises
    ------
    ValueError
        Data has to be 1 keV < E < 500 MeV.

    Returns
    -------
    output : float array
        Given file data transformed to a float np array.

    """
    import re
    import numpy as np
    #End of the header has to be found using regular expressions, since the format slightly changes in each SRIM release.
    header = re.compile('\s*Energy\s*Elec.\s*Nuclear\s*Range\s*Straggling\s*Straggling')
    # Open the file, read in the lines and close it
    file_object = open(filename, "r")
    data = [line.rstrip() for line in file_object]
    file_object.close()
    
    # Get index of first and last data entry    
    # data_index_first = data.index("       Energy      Elec.      Nuclear     Range     Straggling   Straggling") + 2
    
    data_index_first = [i for i, item in enumerate(data) if re.search(header, item)][0]+2
    data_index_last = data.index("-----------------------------------------------------------") - 1 
    
    # Check if first entry is 1 keV and last entry is 100 MeV
    # if not np.round(srim_readline(line=data[data_index_first],eprecision=(1,"keV"),xprecision=(1,"um"))[0]) == 1:
    #     raise ValueError('SRIM File needs start at 1 keV!')
    # if not np.round(srim_readline(line=data[data_index_last],eprecision=(1,"keV"),xprecision=(1,"um"))[0]) == 500000:
    #     raise ValueError('SRIM File needs stop at 500 MeV!')
    
    # Create empty numpy array for output data and add zero entry
    output = np.empty((data_index_last - data_index_first + 2, 6))
    output[0] = [0,0,0,0,0,0]
    
    # Add data to empty np array
    for line in range(data_index_first, data_index_last+1, 1):
        output[line-data_index_first+1] = srim_readline(line=data[line], eprecision=eprecision, xprecision=xprecision)
    
    # Return output data
    return output

def eloss_calc(distance, energy_in, srim_data, precision=(.1,"um")):
    """
    Calculate the energyloss.

    Parameters
    ----------
    distance : float
        Distance the particle has to cross in microns.
    energy_in : float
        Incident energy of the projectile.
    srim_data : float array
        SRIM data array for the material.

    Returns
    -------
    list [e_range, e_energy]
        A list with the range of the particle and the energy after exit.
        If energy is 0 then the range tell how far the particle travelled.
        If energy is not 0 then e_range gives the layer thickness.

    """
    import scipy.interpolate as interpolate
    import numpy as np

    #convert distance / micron to distance/ precision unit
    prefix = {"A": 0.0001, "um": 1., "mm": 1000., "m": 1000000.}
    distance = (distance/prefix[precision[1]])

    # Define interpolation functions with extrapolation to catch 0
    e_to_range = interpolate.interp1d(srim_data[:,0], srim_data[:,3], kind='cubic', fill_value="extrapolate")
    e_to_dedx  = interpolate.interp1d(srim_data[:,0], srim_data[:,1]+srim_data[:,2], kind='cubic', fill_value="extrapolate")
    
    if e_to_range(energy_in) <= distance:
        e_range  = float(e_to_range(energy_in))
        e_energy = 0
    else:
        # Set initial distance and energy for loop
        e_range  = distance
        e_energy = energy_in
        
        # Loop over distance until distance is zero
	# The first element of the precision tuple defines the step width.
	# The unit is taken into account already in the conversion function.
        for i in range(int(distance/precision[0])):
            e_energy -= e_to_dedx(e_energy)
    
    return [e_range, e_energy]

def srim_data_interpolate(srim_data):
    """
    Function to calculate the SRIM interpolations and antiderivatives for later use.

    Parameters
    ----------
    srim_data : TYPE
        Inserted data from the SRIM package. In this case a specific element's stopping power parameters are given.

    Returns
    -------
    list
        List of the 3 interpolated functions: the maximum reach of the particle, the dE/dx conversion and the dE/dx antiderivative.

    """
    import scipy.interpolate as interpolate
    import numpy as np

    # Define interpolation functions with extrapolation to catch 0
        
    e_to_range = interpolate.CubicSpline(srim_data[:,0], srim_data[:,3])
    e_to_dedx  = interpolate.CubicSpline(srim_data[:,0], srim_data[:,1]+srim_data[:,2])
    
    # Calculate the antiderivatives
    e_to_dex_antiderivative  = e_to_dedx.antiderivative()

    return [e_to_range, e_to_dedx, e_to_dex_antiderivative]

def eloss_calc_2(distance, energy_in, srim_data):
    """
    Calculate the energyloss more precise than previously.

    Parameters
    ----------
    distance : float
        Distance the particle has to cross in microns.
    energy_in : float
        Incident energy of the projectile.
    srim_data : float array
        SRIM data array for the material.

    Returns
    -------
    list [e_range, e_energy]
        A list with the range of the particle and the energy after exit.
        If energy is 0 then the range tell how far the particle travelled.
        If energy is not 0 then e_range gives the layer thickness.

    """
    import scipy.interpolate as interpolate
    import numpy as np

    # Define interpolation functions with extrapolation to catch 0
    e_to_range = interpolate.interp1d(srim_data[:,0], srim_data[:,3], kind='cubic', fill_value="extrapolate")
    e_to_dedx  = interpolate.interp1d(srim_data[:,0], srim_data[:,1]+srim_data[:,2], kind='cubic', fill_value="extrapolate") #keV/um
    # e_to_dedx2  = interpolate.interp1d(srim_data[:,0], (srim_data[:,1]+srim_data[:,2])/10, kind='cubic', fill_value="extrapolate") # eV/Angstrom
    
    if e_to_range(energy_in) <= distance:
        e_range  = float(e_to_range(energy_in))
        e_energy = 0
    else:
        # Set initial distance and energy for loop
        e_range  = distance
        e_energy = energy_in
        
        # Loop over distance until distance is zero
        # Make sure to go in 1 micron step as long as the size is not passed
        # Values are passed in microns: int value gives the value in microns:
        # Taking 3 positions after the comma into account.
        distance=int(distance*1000)/1000.
        for j in range(3):
            for i in range(int(distance)):
                if j == 0:
                    e_energy -= e_to_dedx(e_energy)
                    #print('Energy: ' + str(e_energy) + ' keV')
                else:
                    e_energy -= e_to_dedx(e_energy)/(10**j)
                    #print('Energy: ' + str(e_energy) + ' keV')
            distance = (distance-int(distance))*10
            #print(distance)
            
    return [e_range, e_energy]

def particle_range(srim_data):
    import scipy.interpolate as interpolate
    e_to_range = interpolate.interp1d(srim_data[:,0], srim_data[:,3], kind='cubic', fill_value="extrapolate")
    return e_to_range


def eloss_calc_analytical(distance, energy_in, e_to_range, e_to_dex_antiderivative):
    """
    Calculates the energyloss in a specific layer. Does not work...

    Parameters
    ----------
    distance : float
        Distance the particle travels in the specific layer.
    energy_in : float
        Energy the particle has upon entering the layer.
    e_to_range : CubicSpline Object
        Cubic spline object energy vs stopping distance.
    e_to_dex_antiderivative : CubicSpline Object
        Antiderivative of dE/dx with respect to x.

    Returns
    -------
    list
        range the particle can travel through this layer, final energy upon leaving layer.

    """
    import numpy as np
    
    # if particle obviously get halted inside the layer:
    if e_to_range(energy_in) <= distance:
        e_range  = float(e_to_range(energy_in))
        e_energy = 0
    # if particle obviously passed through
    else:
        # Set initial distance and energy for loop
        e_range  = distance
        e_energy = energy_in - (e_to_dex_antiderivative(distance)-e_to_dex_antiderivative(0.))
    
    return [e_range, e_energy]

def removeDuplicates(lst):
    return [t for t in (set(i for i in lst))]

#%% Main part of the script
if __name__ == "__main__":
    # Import modules needed
    import os 
    import numpy as np 
    import itertools

    # Energies to be tested: 
    Energylist = np.arange(1,26) # in MeV
    
    # projectile: proton or deuteron
    projectile = ['16o']#['deuter']#, 'proton']
    # converter elements
    converter = ['Li','Be']#,'LiF','Be','Va','Ta']

    SumOfParameters = list(itertools.product(projectile,converter, Energylist))

    # import SRIM data:
    PathToDataFiles = './SRIM-Data/'
    i=0
    
    # create a Range object for the range of the particles in Matter.
    Range = {}
    for projec in projectile:       
        for ele in converter:
            # define file path:
            Path = PathToDataFiles+projec+'/'+ele+'.txt'
            # import SRIM data:
            Data = srim_import(filename=Path, eprecision=(1,"MeV"), xprecision=(1,"mm"))                
            # Interpolate the ranges in dependecy of the particle energy
            TempRange = particle_range(Data) #output in mm
            Range[projec+ele] = TempRange

#%%    
    # Case 1: Length adjusted according to the Stopping Power relation for individual energies:
    for i, tupel in enumerate(SumOfParameters): 
        tempReaction = tupel[0]+tupel[1]
        Thickness = max(np.floor(float(Range[tempReaction](tupel[2])*1.1)*100)/100, 0.01)
        SumOfParameters[i] = SumOfParameters[i]+(Thickness,) # in mm
        
    # Case 2: Length constant and defined by a maximum value 
    # empty result list
    results = []
    MaximalEnergies = [10,20,30,40,50,60,70,80,95]
    MaximalEnergies = Energylist[np.arange(int(len(Energylist)/4))*4]
    # Calculate the length / thickness for each converter with MaximalEnergies values
    for p in projectile:
        for c in converter:
            for e in MaximalEnergies:
                tempReaction = p+c
                Thickness = np.floor(float(Range[tempReaction](e)*1.1)*100)/100 # in mm
                results.append((p,c,e,Thickness))
    # Upscaling simulations:
    TemporaryResults = []
    for tup in results:
        pr = tup[0]
        co = tup[1]
        th = tup[3]
        for Ekin in Energylist:
            if Ekin<=tup[2]:
                TemporaryResults.append((pr,co,Ekin,th))
        TemporaryResults.append(tup)
    # Join list for both cases
    FinalParamList = removeDuplicates(SumOfParameters + TemporaryResults)
    #%%
    single_file = False
    if single_file:
        # Write list to batch file for cluster deployment.
        myBat = open(r'./autorun.bat','w+')
        for x in FinalParamList:
            myBat.write('call eachrun.bat '+str(x[0])+' '+str(float(x[2])).zfill(5)+'MeV '+str(x[1])+' '+str(x[3])+'\n')
        myBat.close()
        # write list to individuell batch files numbered from 1 to N
    else:
        label = 0
        for x in FinalParamList:
            myBat = open(r'./autorun-dir/'+str(label)+'.bat','w+')
            myBat.write('call eachrun.bat '+str(x[0])+' '+str(float(x[2])).zfill(5)+'MeV '+str(x[1])+' '+str(x[3])+'\n')
            myBat.close()
            label = label+1
            
    #%% Write errorlist
    if False:
        label = 0
        for x in ErrorList:
            myBat = open(r'./autorun-dir/'+str(label)+'.bat','w+')
            myBat.write('call eachrun.bat '+x[1]+' '+x[2]+' '+x[0]+' '+x[3]+'\n')
            myBat.close()
            label = label+1
    #%%
    # Create all energy files needed
    ListOfEnergies = Energylist#+MaximalEnergies
    for en in ListOfEnergies:
        myBat = open(r'./energy/'+str(float(en)).zfill(5)+'MeV.inp','w+')
        myBat.write('e0 = '+str(en))
        myBat.close()
    #%%        
    # Create thickness parameters
    # Thickness defines the length of the converter but also the starting position
    # the target is centered around 0.
    thicknessList = []
    for x in FinalParamList:
        thicknessList.append(x[3])
        
    thicknessList=set(thicknessList)
    for i in thicknessList:
        myGeo = open(r'./geometry/'+str(i)+'.inp','w+')
        myGeo.write('1 RCC 0.00 0.00 '+str(round(-i/20,4))+' 0.00 0.00 '+str(round(i/10,4))+' 2.40')
        myGeo.close()
        
        
        
        
    
        